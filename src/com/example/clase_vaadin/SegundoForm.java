package com.example.clase_vaadin;

import com.google.gwt.layout.client.Layout;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.VaadinRequest;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

@SuppressWarnings("serial")
public class SegundoForm extends VerticalLayout implements View
{
	public static final String NAME = "segundo";

	public SegundoForm()
	{
		this.addComponent(new Label("Soy la segunda ventana"));
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

	
}
