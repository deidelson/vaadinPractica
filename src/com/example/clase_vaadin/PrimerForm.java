package com.example.clase_vaadin;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

@SuppressWarnings("serial")
public class PrimerForm extends VerticalLayout implements View 
{
	public static final String NAME = "";
	
	public PrimerForm()
	{
		Button button = new Button("Abrir segundo formulario");
		button.addClickListener(new Button.ClickListener()
		{
			public void buttonClick(ClickEvent event) 
			{
				//Con esto cree una segunda ventana dentro de la misma pagina
//				Window w = new Window("Segunda ventana");
//				VerticalLayout vl = new VerticalLayout();
//				w.setContent(vl);
//				vl.addComponent(new Label("Soy una nueva ventana"));
//				addWindow(w);
				getUI().getNavigator().navigateTo(SegundoForm.NAME);
				
			}
		});
		addComponent(button); 
	}

	@Override
	public void enter(ViewChangeEvent event) {
		// TODO Auto-generated method stub
		
	}

}
